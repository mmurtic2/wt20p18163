let Tabela = (function(){
    const  iscrtajRaspored = function(div,dani,satPocetak,satKraj){
        if(satPocetak >= satKraj || !Number.isInteger(satPocetak) || 
            !Number.isInteger(satKraj) || !(satPocetak >= 0 && satPocetak <= 24)
            || !(satKraj >= 0 && satKraj <= 24)){
                div.innerHTML = "Greška";
                return;
        }
    
        var i,j;
        var tabela = document.createElement('table');
        var tijelotabele = document.createElement('tbody');
        tabela.appendChild(tijelotabele);
        var sati = [0,2,4,6,8,10,12,15,17,19,21,23];
        var k = satPocetak;
        var tr = document.createElement('tr');
        tijelotabele.appendChild(tr);
        for(i=0; i<(satKraj-satPocetak)+1; i++){
            var nasao = false;
            for(j=0; j<sati.length; j++){
                if(k == sati[j]){
                    nasao = true;
                    break;
                }
            }
            if(nasao && k <= satKraj){
                if (k == satPocetak){
                    var td = document.createElement('td');
                    td.setAttribute("id", "prvisat");
                    td.colSpan = "2";
                    td.appendChild(document.createTextNode(k + ":00"));
                    tr.appendChild(td);
                }else if(k == satKraj){
                    var td = document.createElement('td');
                    td.setAttribute("id", "zadnji");
                    td.appendChild(document.createTextNode(" "));
                    tr.appendChild(td);
                }else{
                    var td = document.createElement('td');
                    td.setAttribute("id", "prvired");
                    td.colSpan = "2";
                    td.appendChild(document.createTextNode(k + ":00"));
                    tr.appendChild(td);
                }
            }else{
                var td = document.createElement('td');
                td.setAttribute("id", "pomocni");
                td.appendChild(document.createTextNode(" "));
                tr.appendChild(td);
                var td1 = document.createElement('td');
                td1.setAttribute("id", "pomocni");
                td1.appendChild(document.createTextNode(" "));
                tr.appendChild(td1);
            }
            k+= 1;
        }
        var m = 0;
        for(i=0; i<dani.length; i++){
            var tr1 = document.createElement('tr');
            tijelotabele.appendChild(tr1);
            m = 0;
            for(j=0; j<(satKraj-satPocetak)*2+1; j++){
                if(j == 0){
                    if(i==0){
                        var td = document.createElement('td');
                        td.setAttribute("id", "pon");
                        td.appendChild(document.createTextNode(dani[i]));
                        tr1.appendChild(td);
                    }else{
                        var td = document.createElement('td');
                        td.setAttribute("id", "dani");
                        td.appendChild(document.createTextNode(dani[i]));
                        tr1.appendChild(td);
                    }
                }else{
                    if(m%2==0){
                        var td = document.createElement('td');
                        td.setAttribute("id", "isprekidana");
                        tr1.appendChild(td);
                    }else {
                        var td = document.createElement('td');
                        td.setAttribute("id", "puna");
                        tr1.appendChild(td);
                    }
                    m++;
                }
            }
        }
        div.appendChild(tabela);
    }

    const dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj,dan){
        if(typeof(raspored) == 'undefined' && raspored == null){
            alert('Greška - raspored nije kreiran');
            return 'Greška - raspored nije kreiran';
        }
        
        var i, j;
        var tabela = raspored.childNodes[0];
        var brojRedovaTabele = tabela.rows;
        var nasao = false;
        for(i=1; i<brojRedovaTabele.length; i++){
            if(brojRedovaTabele[i].getElementsByTagName("td")[0].innerHTML == dan){
                nasao = true;
                break;
            }
        }
    
        var decimalePocetak = vrijemePocetak - Math.floor(vrijemePocetak);
        var decimaleKraj =  vrijemeKraj - Math.floor(vrijemeKraj);
        if(vrijemeKraj <= vrijemePocetak || !nasao || (decimalePocetak != 0.5 && !Number.isInteger(vrijemePocetak))
        || (decimaleKraj != 0.5 && !Number.isInteger(vrijemeKraj)) ||
            (vrijemePocetak < parseInt(brojRedovaTabele[0].getElementsByTagName("td")[0].innerHTML))){
            alert('Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin');
            return 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin';
        }
    
        var red;
        for(i=1; i<brojRedovaTabele.length; i++){
            if(brojRedovaTabele[i].getElementsByTagName("td")[0].innerHTML == dan){
                red = i;
                break;
            }
        }
        var razlika = parseInt((vrijemeKraj-vrijemePocetak)*2);
        var ovdje = (vrijemePocetak - parseInt(brojRedovaTabele[0].getElementsByTagName("td")[0].innerHTML))*2;
        var pom = razlika;
        var kraj = (vrijemeKraj - parseInt(brojRedovaTabele[0].getElementsByTagName("td")[0].innerHTML))*2;
       
        for(i=1; i<=kraj; i++){
            if(brojRedovaTabele[red].cells[i].id == "aktivnost"){
                var br = brojRedovaTabele[red].cells[i].colSpan;
                if((i>= ovdje+1 && i<= kraj) || (i+br-1 >= ovdje+1 && i+br-1 <= kraj)){
                    alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                    return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
                }
            }
        }
        
        
        var x = ovdje+razlika+1;
        for(i=ovdje+2; i<x; i++){
            brojRedovaTabele[red].getElementsByTagName("td")[i].setAttribute("id", "sakrij");
            razlika--;
            if(razlika == 1){
                break;
            }
        }
    
        if(Number.isInteger(vrijemeKraj)){
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightStyle = "solid";
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightWidth = "1px";
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightColor = "black";
        }else{
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightStyle = "dashed";
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightWidth = "1px";
            brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].style.borderRightColor = "black";
        }
        
        brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].colSpan = pom;
        brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].setAttribute("id", "aktivnost");
        brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].appendChild(document.createTextNode(naziv));
        brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].appendChild(document.createElement('br'));
        brojRedovaTabele[red].getElementsByTagName("td")[ovdje+1].appendChild(document.createTextNode(tip));    
    }

    return{
        iscrtajRaspored : iscrtajRaspored,
        dodajAktivnost : dodajAktivnost
    }
    
}());