
window.alert = function(){

};

let assert = chai.assert;
describe('Tabela', function(){
    describe('iscrtajRaspored()', function(){
        it('Treba nacrtati 6 redova', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi.length, 6,"Broj redova treba biti 6");
            okvir.innerHTML = "";
        });
        it('Treba nacrtati 27 kolona', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells.length, 27,"Broj kolona treba biti 27");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer je satPočetak > satKraj', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],21,8);
            assert.equal(okvir.innerHTML, "Greška","Treba ispisati grešku");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer satPočetak nije cijeli broj', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8.5,21);
            assert.equal(okvir.innerHTML, "Greška","Treba ispisati grešku");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer satKraj nije cijeli broj', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21.3);
            assert.equal(okvir.innerHTML, "Greška","Treba ispisati grešku");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer satKraj nije broj od 0 do 24', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,25);
            assert.equal(okvir.innerHTML, "Greška","Treba ispisati grešku");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer satPočetak nije broj od 0 do 24', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],-8,21);
            assert.equal(okvir.innerHTML, "Greška","Treba ispisati grešku");
            okvir.innerHTML = "";
        });
        it('Treba vratiti vrijednost satPocetak koja je 8:00', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[0].cells[0].innerHTML, "8:00","Treba vratiti 8:00");
            okvir.innerHTML = "";
        });
        it('Treba vratiti vrijednost Ponedjeljak', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells[0].innerHTML, "Ponedjeljak","Treba vratiti Ponedjeljak");
            okvir.innerHTML = "";
        });
        it('Treba vratiti vrijednost Petak', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[redovi.length-1].cells[0].innerHTML, "Petak","Treba vratiti Petak");
            okvir.innerHTML = "";
        });
        it('Treba nacrtati 3 reda', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak"],12,17);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi.length, 3,"Broj redova treba biti 3");
            okvir.innerHTML = "";
        });
        it('Treba nacrtati 10 kolona', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak"],12,17);
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells.length, 11,"Broj kolona treba biti 10");
            okvir.innerHTML = "";
        });
    });

    describe('dodajAktivnost()', function(){
        it('Treba vratiti WT predavanje', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            Tabela.dodajAktivnost(okvir,"WT","predavanje",9,12,"Ponedjeljak");
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells[3].innerHTML, "WT<br>predavanje","Treba vratiti WT predavanje");
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer početak aktivnosti nema u rasporedu', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let m = Tabela.dodajAktivnost(okvir,"WT","predavanje",6,12,"Ponedjeljak");
            assert.equal(m , 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin',"Treba vratiti grešku");
        });
        it('Treba ispisati grešku jer traženi dan nema u rasporedu', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let m = Tabela.dodajAktivnost(okvir,"WT","predavanje",6,12,"Subota");
            assert.equal(m , 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin',"Treba vratiti grešku");
        });
        it('Treba ispisati grešku jer je vrijemeKraj <= vrijemePocetak', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let m = Tabela.dodajAktivnost(okvir,"WT","predavanje",12,8,"Petak");
            assert.equal(m , 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin',"Treba vratiti grešku");
        });
        it('Treba ispisati grešku jer vrijemePočetak ima decimalni dio koji nije 0.5', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let m = Tabela.dodajAktivnost(okvir,"WT","predavanje",9.2,13,"Petak");
            assert.equal(m , 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin',"Treba vratiti grešku");
        });
        it('Treba ispisati grešku jer vrijemeKraj ima decimalni dio koji nije 0.5', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            let m = Tabela.dodajAktivnost(okvir,"WT","predavanje",9,13.7,"Petak");
            assert.equal(m , 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin',"Treba vratiti grešku");
        });
        it('Treba ispisati grešku jer već postoji termin u zadanom vremenu', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            Tabela.dodajAktivnost(okvir,"WT","predavanje",9,13.5,"Petak");
            let m = Tabela.dodajAktivnost(okvir,"WT","vjezbe",12,15,"Petak");
            assert.equal(m , 'Greška - već postoji termin u rasporedu u zadanom vremenu');
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer već postoji termin u zadanom vremenu', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,21);
            Tabela.dodajAktivnost(okvir,"WT","predavanje",19,20.5,"Petak");
            let m = Tabela.dodajAktivnost(okvir,"WT","vjezbe",19,20.5,"Petak");
            assert.equal(m , 'Greška - već postoji termin u rasporedu u zadanom vremenu');
            okvir.innerHTML = "";
        });
        it('Treba ispisati grešku jer već postoji termin u zadanom vremenu', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak","Srijeda","Četvrtak","Petak"],8,23);
            Tabela.dodajAktivnost(okvir,"WT","predavanje",19,20.5,"Petak");
            let m = Tabela.dodajAktivnost(okvir,"WT","vjezbe",20,22,"Petak");
            assert.equal(m , 'Greška - već postoji termin u rasporedu u zadanom vremenu');
            okvir.innerHTML = "";
        });
        it('Treba vratiti WT predavanje', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak"],12,23);
            Tabela.dodajAktivnost(okvir,"WT","predavanje",12,15,"Ponedjeljak");
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells[1].innerHTML, "WT<br>predavanje","Treba vratiti WT predavanje");
            okvir.innerHTML = "";
        });
        it('Treba vratiti WT vježbe', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak"],12,23);
            Tabela.dodajAktivnost(okvir,"WT","vježbe",20,23,"Ponedjeljak");
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells[17].innerHTML, "WT<br>vježbe","Treba vratiti WT vježbe");
            okvir.innerHTML = "";
        });
        it('Treba vratiti VVS vježbe', function(){
            let okvir=document.getElementById("tabela");
            Tabela.iscrtajRaspored(okvir,["Ponedjeljak","Utorak"],12,23);
            Tabela.dodajAktivnost(okvir,"VVS","vježbe",20.5,22.5,"Ponedjeljak");
            let redovi = okvir.getElementsByTagName("tr");
            assert.equal(redovi[1].cells[18].innerHTML, "VVS<br>vježbe","Treba vratiti VVS vježbe");
            okvir.innerHTML = "";
        });
    });
});