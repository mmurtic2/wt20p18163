const express = require('express');
var fs = require('fs');
const app = express();
app.use(express.static('public'));
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password:'root',
    database:'wt2018163'
});

app.get('/v1/predmeti', function(req,res){
    fs.readFile('predmeti.txt', function(err, data){
        if(err) throw err;
        var niz = data.toString();
        niz = niz.split('\n');
        var rezultat = [];
        for(i=1; i<niz.length; i++){
            var pom =  {naziv : niz[i]};
            rezultat.push(pom);
        }        
        res.json(rezultat);
    });
});
    

app.get('/v1/aktivnosti', function(req,res){
    fs.readFile('aktivnosti.txt', function(err, data){
        if(err) throw err;
        var niz = data.toString();
        niz = niz.split('\n');
        var rezultat = [];
        for(i=1; i<niz.length; i++){
            var p = niz[i].split(',');
            var pom =  {naziv:p[0],tip:p[1],pocetak:p[2],kraj:p[3],dan:p[4]};
            rezultat.push(pom);
        }
        res.json(rezultat);
    });
});


app.get('/v1/predmet/:naziv/aktivnost', function(req,res){
    fs.readFile('aktivnosti.txt', function(err, data){
        if(err) throw err;
        var predmet = req.url.split('/');
        var niz = data.toString();
        niz = niz.split('\n');
        var rezultat = [];
        for(i=1; i<niz.length; i++){
            var p = niz[i].split(',');
            if (predmet[2] == p[0]){
                var pom =  {naziv:p[0],tip:p[1],pocetak:p[2],kraj:p[3],dan:p[4]};
                rezultat.push(pom);
            }
        }
        res.json(rezultat);
    });
});

app.post('/v1/predmet',function(req,res){
    let tijelo = req.body;
    var vecPostoji = false;
    fs.readFile('predmeti.txt', function(err, data){
        if(err) throw err;
        var niz = data.toString();
        niz = niz.split('\n');
        for(i=1; i<niz.length; i++){
            var p = niz[i];
            if (p == tijelo['naziv']){
                vecPostoji = true;
                break;
            }
        }      
        if(vecPostoji){
            return res.json({message:"Naziv predmeta postoji!"});
        }else{
            let novaLinija = "\n" + tijelo['naziv'];
            fs.appendFile('predmeti.txt',novaLinija,function(err){
                if(err) throw err;
                return res.json({message:"Uspješno dodan predmet!",data:novaLinija});
            });
        } 
    });
 });

 app.post('/v1/aktivnost',function(req,res){
    fs.readFile('aktivnosti.txt', function(err, data){
        if(err) throw err;
        let tijelo = req.body;
        var greska = false;
        var niz = data.toString();
        niz = niz.split('\n');
        for(i=1; i<niz.length; i++){
            var p2 = niz[i].split(',');
            if((parseInt(p2[2]) == parseInt(tijelo['pocetak']) && p2[4] == tijelo['dan']) ||
                (parseInt(p2[3]) == parseInt(tijelo['kraj']) && p2[4] == tijelo['dan']) ||
                (parseInt(p2[2]) <= parseInt(tijelo['pocetak']) && parseInt(p2[3]) >= parseInt(tijelo['kraj']) && p2[4] == tijelo['dan']) ||
                (parseInt(p2[2]) >= parseInt(tijelo['pocetak']) && parseInt(p2[3]) <= parseInt(tijelo['kraj']) && p2[4] == tijelo['dan']) || 
                (parseInt(tijelo['pocetak']) > parseInt(p2[2]) && parseInt(tijelo['pocetak']) < parseInt(p2[3]) && p2[4] == tijelo['dan']) ||
                (parseInt(tijelo['kraj']) > parseInt(p2[2]) && parseInt(tijelo['kraj']) < parseInt(p2[3]) && p2[4] == tijelo['dan'])){
                    greska = true;
                    break;
            }
        }
        if (parseInt(tijelo['pocetak']) < 8 || parseInt(tijelo['kraj']) > 20 || 
        parseInt(tijelo['kraj']) <= parseInt(tijelo['pocetak'])){
        greska = true;
    }
        if(greska){
            return res.json({message:"Aktivnost nije validna!"});
        }else {
            let novaLinija = "\n" + tijelo['naziv'] + ',' + tijelo['tip'] + ',' + tijelo['pocetak'] + ',' + tijelo['kraj'] + ',' + tijelo['dan'];
            fs.appendFile('aktivnosti.txt',novaLinija,function(err){
                if(err) throw err;
                return res.json({message:"Uspješno dodana aktivnost!",data:novaLinija});
            });
        }
    });
 });


 app.delete('/v1/aktivnost/:naziv',function(req,res){
    var pom = req.url.split('/');
    fs.readFile('aktivnosti.txt', function(err, data){
        if(err) throw err;
        var niz = data.toString();
        niz = niz.split('\n');
        var obrisan = false;
        for(i=1; i<niz.length; i++){
            var p1 = niz[0].split(',');
            var p2 = niz[i].split(',');
            if(pom[2] == p2[0]){
                niz.splice(i,1);
                obrisan = true;
                i = i - 1;
            }
        }
        niz = niz.join('\n');
        fs.writeFile('aktivnosti.txt', niz.toString(), function(err){
            if(err) throw err;
        });
        if(obrisan) return res.json({message:"Uspješno obrisana aktivnost!"});
        else return res.json({message:"Greška - aktivnost nije obrisana!"});
    });
});


app.delete('/v1/predmet/:naziv',function(req,res){
    var predmet = req.url.split('/');
    fs.readFile('predmeti.txt', function(err, data){
        if(err) throw err;
        var niz = data.toString();
        niz = niz.split('\n');
        var obrisan = false;
        for(i=1; i<niz.length; i++){
            if(predmet[2] == niz[i]){
                niz.splice(i,1);
                obrisan = true;
                break;
            }
        }        
        niz = niz.join('\n');
        fs.writeFile('predmeti.txt', niz.toString(), function(err){
            if(err) throw err;
        });
        if(obrisan) return res.json({message:"Uspješno obrisan predmet!"});
        else return res.json({message:"Greška - predmet nije obrisan!"});
    });
 });

 app.delete('/v1/all',function(req,res){
    var aktivnosti = "naziv,tip,pocetak,kraj,dan";
    fs.writeFile('aktivnosti.txt', aktivnosti, function(err){
        if(err) throw err;
    });
    var predmeti = "naziv";
    fs.writeFile('predmeti.txt', predmeti, function(err){
        if(err) return res.json({message:"Greška - sadržaj datoteka nije moguće obrisati!"});
    });
    return res.json({message:"Uspješno obrisan sadržaj datoteka!"});
 });


 
const Sequelize = require("sequelize");
const { aktivnost, predmet } = require('./db');
const sequelize = new Sequelize("wt2018163","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;
Predmet = require(__dirname+'/predmet.js')(sequelize, Sequelize.DataTypes);
Grupa = require(__dirname+'/grupa.js')(sequelize, Sequelize.DataTypes);
Aktivnost = require(__dirname+'/aktivnost.js')(sequelize, Sequelize.DataTypes);
Dan = require(__dirname+'/dan.js')(sequelize, Sequelize.DataTypes);
Tip = require(__dirname+'/tip.js')(sequelize, Sequelize.DataTypes);
Student = require(__dirname+'/student.js')(sequelize, Sequelize.DataTypes);

 //CRUD operacije za dans
app.post('/v2/dan', function(req,res){
    Dan.findOrCreate({where:{
        naziv : req.body['naziv']
    }}); 
    res.json();
});

app.get('/v2/dani', function(req, res){
    Dan.findAll().then(dan => res.json(dan));
});

app.get('/v2/dan/:id', function(req, res){
    let ID = req.url.split('/');
    Dan.findOne({where:{id : ID[3]}}).then(dan => res.json(dan));
});

app.put('/v2/dan/:id', function(req, res){
    let ID = req.url.split('/');
    Dan.update(
        {naziv : req.body['naziv']},
        {where: {id : ID[3]}}
    );
    res.json();
});

app.delete('/v2/dan/:id', function(req,res){
    let ID = req.url.split('/');
    Dan.destroy({where:{id: ID[3]}});
    res.json();
});

//CRUD operacije za predmets
app.post('/v2/predmet', function(req,res){
    Predmet.findOrCreate({where:{
        naziv : req.body['naziv']
    }}); 
    res.json();
});

app.get('/v2/predmeti', function(req, res){
    Predmet.findAll().then(predmet => res.json(predmet));
});

app.get('/v2/predmet/:id', function(req, res){
    let ID = req.url.split('/');
    Predmet.findOne({where:{id : ID[3]}}).then(predmet => res.json(predmet));
});

app.put('/v2/predmet/:id', function(req, res){
    let ID = req.url.split('/');
    Predmet.update(
        {naziv : req.body['naziv']},
        {where: {id : ID[3]}}
    );
    res.json();
});

app.delete('/v2/predmet/:id', function(req,res){
    let ID = req.url.split('/');
    Predmet.destroy({where:{id: ID[3]}});
    res.json();
});

//CRUD operacije za tips
app.post('/v2/tip', function(req,res){
    Tip.findOrCreate({where:{
        naziv : req.body['naziv']
    }});
    res.json();
});

app.get('/v2/tipovi', function(req, res){
    Tip.findAll().then(tip => res.json(tip));
});

app.get('/v2/tip/:id', function(req, res){
    let ID = req.url.split('/');
    Tip.findOne({where:{id : ID[3]}}).then(tip => res.json(tip));
});

app.put('/v2/tip/:id', function(req, res){
    let ID = req.url.split('/');
    Tip.update(
        {naziv : req.body['naziv']},
        {where: {id : ID[3]}}
    );
    res.json();
});

app.delete('/v2/tip/:id', function(req,res){
    let ID = req.url.split('/');
    Tip.destroy({where:{id: ID[3]}});
    res.json();
});

//CRUD za students
app.post('/v2/student', function(req,res){
    Student.findOrCreate({where:{
        ime : req.body['ime'],
        indeks : req.body['indeks']
    }});
    res.json();
});


app.get('/v2/studenti', function(req, res){
    Student.findAll().then(student => res.json(student));
});


app.get('/v2/student/:id', function(req, res){
    let ID = req.url.split('/');
    Student.findOne({where:{id : ID[3]}}).then(student => res.json(student));
});

app.put('/v2/student/:id', function(req, res){
    let ID = req.url.split('/');
    if(req.body['ime'] != null && req.body['indeks'] != null){
        Student.update(
            {ime : req.body['ime'],
            indeks : req.body['indeks']},
            {where: {id : ID[3]}}
        );
    }else if(req.body['ime'] != null){
        Student.update(
            {ime : req.body['ime']},
            {where: {id : ID[3]}}
        );
    }else if(req.body['indeks']){
        Student.update(
            {indeks : req.body['indeks']},
            {where: {id : ID[3]}}
        );
    }
    res.json();
});

app.delete('/v2/student/:id', function(req,res){
    let ID = req.url.split('/');
    Student.destroy({where:{id: ID[3]}});
    res.json();
});

//CRUD operacije za grupas
app.post('/v2/grupa', function(req,res){
    let uspjesno = true;
    let pom = [req.body['naziv'], new Date(), new Date(), req.body['PredmetId']];
    let sql = "SELECT * FROM grupas";
    connection.query(sql, (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            if (red.naziv == req.body['naziv'] && red.PredmetId == req.body['PredmetId']){
                uspjesno = false;
                break;
            }
        }
        if(uspjesno){
            let imaVec  = false;
            let sql = "SELECT * FROM predmets";
            connection.query(sql, (err, result, fields) =>{
                if(err) throw err;
                for(red of result){
                    if (red.id == req.body['PredmetId']){
                        imaVec = true;
                        break;
                    }
                }
                if(imaVec){
                    let sql = "INSERT INTO grupas (naziv, createdAt, updatedAt, PredmetId) VALUES (?,?,?,?)";
                    connection.query(sql, pom, (err, result, fields) =>{
                        if(err) throw err;
                        res.json({message: 'Uspješno dodana grupa!'});
                    });    
                }else res.json({message: 'Željeni predmet ne postoji!'});
                
            });
        }else res.json({message: 'Grupa sa ovim nazivom već postoji!'});
    });
});



app.get('/v2/grupe', function(req, res){
    Grupa.findAll().then(grupa => res.json(grupa));
});

app.get('/v2/grupa/:id', function(req, res){
    let ID = req.url.split('/');
    let rez = [];
    let sql = "SELECT * FROM grupas WHERE id = ?";
    connection.query(sql, ID[3], (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            pom = {id:red.id,naziv:red.naziv,PredmetId:red.PredmetId};
            rez.push(pom);
        }
        res.json(rez);
    });
});

app.put('/v2/grupa/:id', function(req, res){
    let ID = req.url.split('/');
    let vecPostoji = false;
    if (req.body['PredmetId'] != null){
        let sql = "SELECT * FROM predmets";
        connection.query(sql, (err, result, fields) =>{
            if(err) throw err;
            if(result.length.toString() >= req.body['PredmetId']){
                vecPostoji = true;
            }
            if(vecPostoji){
                if(req.body['naziv'] != null){
                    let sql = "UPDATE grupas SET naziv = ?, PredmetId = ? WHERE id = ?";
                    connection.query(sql, [req.body['naziv'], req.body['PredmetId'],ID[3]], (err, result, fields) =>{
                        if(err) throw err;
                        res.json({message:'Uspješno ste UPDATE grupu'});
                    });
                }else{
                    let sql = "UPDATE grupas SET PredmetId = ? WHERE id = ?";
                    connection.query(sql, [req.body['PredmetId'],ID[3]], (err, result, fields) =>{
                        if(err) throw err;
                        res.json({message:'Uspješno ste UPDATE grupu'});
                    });
                }
            }else res.json({message:'Željeni predmet ne postoji!'});
        });
    }else if(req.body['naziv'] != null){
        let sql = "UPDATE grupas SET naziv = ? WHERE id = ?";
        connection.query(sql, [req.body['naziv'], ID[3]], (err, result, fields) =>{
            if(err) throw err;
            res.json({message:'Uspješno ste UPDATE grupu'});
        });
    }
});


app.delete('/v2/grupa/:id', function(req,res){
    let ID = req.url.split('/');
    Grupa.destroy({where:{id: ID[3]}});
    res.json();
});

//vraća sve predmete kod kojih je PredmetId jednaka id iz rute
app.get('/v2/predmet/:id/grupa', function(req, res){
    let ID = req.url.split('/');
    Grupa.findAll({where:{PredmetId : ID[3]}}).then(predmet => res.json(predmet));
});



//CR za student_grupa
app.post('/v2/student_grupa', function(req, res){
    let uspjesno = true;
    let pom = [new Date(), new Date(), req.body['GrupaId'], req.body['StudentId']];
    let sql = "SELECT * FROM student_grupa";
    connection.query(sql, (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            if (red.GrupaId == req.body['GrupaId'] && red.StudentId == req.body['StudentId']){
                uspjesno = false;
                break;
            }
        }
        if(uspjesno){
            let sql = "SELECT * FROM students";
            let vecPostoji = false;
            connection.query(sql, (err, result, fields) =>{
                if(err) throw err;
                for(red of result){
                    if (red.id == req.body['StudentId']){
                        vecPostoji = true;
                        break;
                    }
                }
                if(vecPostoji){
                    let sql = "SELECT * FROM grupas";
                    let vecPostoji = false;
                    connection.query(sql, (err, result, fields) =>{
                        if(err) throw err;
                        for(red of result){
                            if (red.id == req.body['GrupaId']){
                                vecPostoji = true;
                                break;
                            }
                        }
                        if(vecPostoji){
                            let sql = "INSERT INTO student_grupa (createdAt, updatedAt, GrupaId, StudentId) VALUES (?,?,?,?)";
                            connection.query(sql, pom, (err, result, fields) =>{
                                if(err) throw err;
                                res.json({message: 'Uspješno ste dodali vezu između studenta i grupe!'});
                            });
                        }else res.json({message: 'Ne postoji grupa s ovim Id!'});
                    });
                }else res.json({message: 'Ne postoji student s ovim Id!'});
            });
            //mada se ne mogu dodati dva ista reda zbog pk
        }else res.json({message: 'Ova veza studenta i grupe već postoji!'});
    });

});

app.get('/v2/studenti_grupe', function(req, res){
    let rez = [];
    let sql = "SELECT * FROM student_grupa";
    connection.query(sql, (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            pom = {GrupaId:red.GrupaId,StudentId:red.StudentId};
            rez.push(pom);
        }
        res.json(rez);
    });
});

//vraca sve parove kod kojih je StudentId jedan id iz rute
app.get('/v2/student/:id/student_grupa', function(req, res){
    let rez = [];
    let ID = req.url.split('/');
    let sql = "SELECT * FROM student_grupa WHERE StudentId = ?";
    connection.query(sql,ID[3], (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            pom = {GrupaId:red.GrupaId,StudentId:red.StudentId};
            rez.push(pom);
        }
        res.json(rez);
    });
});

//vraca sve parove kod kojih je GrupaId jedan id iz rute
app.get('/v2/grupa/:id/student_grupa', function(req, res){
    let rez = [];
    let ID = req.url.split('/');
    let sql = "SELECT * FROM student_grupa WHERE GrupaId = ?";
    connection.query(sql,ID[3], (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            pom = {GrupaId:red.GrupaId,StudentId:red.StudentId};
            rez.push(pom);
        }
        res.json(rez);
    });
});


//CRUD za aktivnosts
app.post('/v2/aktivnost', function(req, res){
    let uspjesno = true;
    let pom = [req.body['naziv'], req.body['pocetak'], req.body['kraj'], new Date(), new Date(), req.body['PredmetId'], req.body['GrupaId'], req.body['DanId'], req.body['TipId']];    
    let sql = "SELECT * FROM aktivnosts";
    connection.query(sql, (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            if ((red.pocetak == req.body['pocetak'] && red.dan == req.body['dan']) || 
                (red.kraj == req.body['kraj'] && red.dan == req.body['dan']) ||
                (red.pocetak < req.body['pocetak'] && red.kraj > req.body['kraj'] && red.dan == req.body['dan']) ||
                (red.pocetak > req.body['pocetak'] && red.kraj < req.body['kraj'] && red.dan == req.body['dan']) ||
                (red.pocetak < req.body['pocetak'] && red.kraj > req.body['pocetak'] && red.dan == req.body['dan']) ||
                (red.pocetak < req.body['kraj'] && red.kraj > req.body['kraj'] && red.dan == req.body['dan'])){
                uspjesno = false;
                break;
            }
        }
        if(uspjesno){
            let sql = "SELECT * FROM predmets";
            let vecPostoji = false;
            connection.query(sql, (err, result, fields) =>{
                if(err) throw err;
                for(red of result){
                    if (red.id == req.body['PredmetId']){
                        vecPostoji = true;
                        break;
                    }
                }
                if(vecPostoji){
                    let sql = "SELECT * FROM grupas";
                    let vecPostoji = false;
                    connection.query(sql, (err, result, fields) =>{
                        if(err) throw err;
                        for(red of result){
                            if (red.id == req.body['GrupaId']){
                                vecPostoji = true;
                                break;
                            }
                        }
                        if(vecPostoji){
                            let sql = "SELECT * FROM dans";
                            let vecPostoji = false;
                            connection.query(sql, (err, result, fields) =>{
                                if(err) throw err;
                                for(red of result){
                                    if (red.id == req.body['DanId']){
                                        vecPostoji = true;
                                        break;
                                    }
                                }
                                if(vecPostoji){
                                    let sql = "SELECT * FROM tips";
                                    let vecPostoji = false;
                                    connection.query(sql, (err, result, fields) =>{
                                        if(err) throw err;
                                        for(red of result){
                                            if (red.id == req.body['TipId']){
                                                vecPostoji = true;
                                                break;
                                            }
                                        }
                                        if(vecPostoji){
                                            let sql = "INSERT INTO aktivnosts (naziv, pocetak, kraj, createdAt, updatedAt, PredmetId, GrupaId, DanId, TipId) VALUES (?,?,?,?,?,?,?,?,?)";
                                            connection.query(sql, pom, (err, result, fields) =>{
                                                if(err) throw err;
                                                res.json({message: 'Uspješno ste dodali aktivnost!'});
                                            });
                                        }else res.json({message: 'Ne postoji tip sa ovim Id!'});
                                    });
                                }else res.json({message: 'Ne postoji dan sa ovim Id!'});
                            });
                        }else res.json({message: 'Ne postoji grupa sa ovim Id!'});
                    });
                }else res.json({message: 'Ne postoji predmet sa ovim Id!'});
            });
        }else res.json({message: 'Već postoji aktivnost u ovom periodu!'});
    });
});

//vraca sve aktivnosti ciji je id jednak id iz rute
app.get('/v2/aktivnosti/:id', function(req, res){
    let ID = req.url.split('/');
    Aktivnost.findOne({where:{id : ID[3]}}).then(aktivnost => res.json(aktivnost));
});


//vraca sve aktivnosti ciji je Predmetid jednak id iz rute
app.get('/v2/predmet/:id/aktivnost', function(req, res){
    let ID = req.url.split('/');
    Aktivnost.findOne({where:{PredmetId : ID[3]}}).then(aktivnost => res.json(aktivnost));
});

//vraca sve aktivnosti ciji je GrupaId jednak id iz rute
app.get('/v2/grupa/:id/aktivnost', function(req, res){
    let ID = req.url.split('/');
    Aktivnost.findOne({where:{GrupaId : ID[3]}}).then(aktivnost => res.json(aktivnost));
});

//vraca sve aktivnosti ciji je DanId jednak id iz rute
app.get('/v2/dan/:id/aktivnost', function(req, res){
    let ID = req.url.split('/');
    Aktivnost.findOne({where:{DanId : ID[3]}}).then(aktivnost => res.json(aktivnost));
});


//vraca sve aktivnosti ciji je TipId jednak id iz rute
app.get('/v2/tip/:id/aktivnost', function(req, res){
    let ID = req.url.split('/');
    Aktivnost.findOne({where:{TipId : ID[3]}}).then(aktivnost => res.json(aktivnost));
});

app.get('/v2/aktivnosti', function(req, res){
    Aktivnost.findAll().then(aktivnost => res.json(aktivnost));
});
 
app.put('/v2/aktivnost/:id', function(req, res){
    let ID = req.url.split('/');
    let sql = "SELECT * FROM predmets";
    let vecPostoji = false;
    connection.query(sql, (err, result, fields) =>{
        if(err) throw err;
        for(red of result){
            if (red.id == req.body['PredmetId']){
                vecPostoji = true;
                break;
            }
        }
        if(vecPostoji){
            let sql = "SELECT * FROM grupas";
            let vecPostoji = false;
            connection.query(sql, (err, result, fields) =>{
                if(err) throw err;
                for(red of result){
                    if (red.id == req.body['GrupaId']){
                        vecPostoji = true;
                        break;
                    }
                }
                if(vecPostoji){
                    let sql = "SELECT * FROM dans";
                    let vecPostoji = false;
                    connection.query(sql, (err, result, fields) =>{
                        if(err) throw err;
                        for(red of result){
                            if (red.id == req.body['DanId']){
                                vecPostoji = true;
                                break;
                            }
                        }
                        if(vecPostoji){
                            let sql = "SELECT * FROM tips";
                            let vecPostoji = false;
                            connection.query(sql, (err, result, fields) =>{
                                if(err) throw err;
                                for(red of result){
                                    if (red.id == req.body['TipId']){
                                        vecPostoji = true;
                                        break;
                                    }
                                }
                                if(vecPostoji){
                                    let sql = "UPDATE aktivnosts SET naziv = ?, pocetak = ?, kraj = ?, PredmetId=?, GrupaId=?,  DanId=?, TipId=? WHERE id = ?";
                                    connection.query(sql, [req.body['naziv'], req.body['pocetak'], req.body['kraj'], req.body['PredmetId'], req.body['GrupaId'], req.body['DanId'], req.body['TipId'], ID[3]], (err, result, fields) =>{
                                        if(err) throw err;
                                        res.json({message:'Uspješno ste UPDATE aktivnost'});
                                    });
                                }else res.json({message: 'Ne postoji tip sa ovim Id!'});
                            });
                        }else res.json({message: 'Ne postoji dan sa ovim Id!'});
                    });
                }else res.json({message: 'Ne postoji grupa sa ovim Id!'});
            });
        }else res.json({message: 'Ne postoji predmet sa ovim Id!'});
    });  
});


app.delete('/v2/tip/:id', function(req,res){
    let ID = req.url.split('/');
    Aktivnost.destroy({where:{id: ID[3]}});
    res.json();
});

app.listen(3000);
module.exports = app;