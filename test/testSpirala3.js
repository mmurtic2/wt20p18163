const chai = require('chai');
var fs = require('fs');
const chaiHttp = require('chai-http');
const { json } = require('express');
const server = require('../serverskaSkripta');
const { get } = require('../serverskaSkripta');
let should = chai.should();
chai.use(chaiHttp);


const data = fs.readFileSync('testniPodaci.txt', {encoding:'utf8'});

var niz = data.split('\n');
var nizPodataka = [];
for(j=0; j<niz.length; j++){
    jedanRed = niz[j].split(',');
    operacija1 = jedanRed[0];
    ruta1 = jedanRed[1];
    ulaz1 = jedanRed[2];
    izlaz1 = jedanRed[3];
    suma = 0;
    if(operacija1 == 'GET'){
        k = 3;
        if(izlaz1 != '[]'){
            while(jedanRed[k] != undefined){
                suma++;
                k++;
            }
        }
         if(ruta1 != '/predmeti'){
            //zato što jedna aktivnost ima 4 zareza
            if(suma % 5 == 0) suma = suma - (suma%4);                 
            suma = suma / 4;
        }
        //kao izlaz sam brojala koliko ima predmeta ili aktivnosti
        var pom =  {operacija:operacija1,ruta:ruta1,ulaz:ulaz1,izlaz:suma};
    }else{
        if(operacija1 == 'POST' && ruta1 == '/predmet'){
            d = ulaz1.split('\\');
            f = d[3].split('"');
            ulaz1 = f[1];
        }if(operacija1 == 'POST' && ruta1 == '/aktivnost'){
            var p1 = jedanRed[2].split('\\');
            var r1 = p1[3].split('"');
            var p2 = jedanRed[3].split('\\');
            var r2 = p2[3].split('"');
            var p3 = jedanRed[4].split('\\');
            var r3 = p3[2].split(':');
            var p4 = jedanRed[5].split('\\');
            var r4 = p4[2].split(':');
            var p5 = jedanRed[6].split('\\');
            var r5 = p5[3].split('"');
            ulaz1 = {naziv:r1[1],tip:r2[1],pocetak:r3[1],kraj:r4[1],dan:r5[1]};
            izlaz1 = jedanRed[7];
        }
        var p = izlaz1.split('\\');
        p = p[3].split('"');
        izlaz1 = p[1];
        var pom =  {operacija:operacija1,ruta:ruta1,ulaz:ulaz1,izlaz:izlaz1};
    }
    nizPodataka.push(pom);
}      


function test(i){
    if (nizPodataka[i].operacija == 'GET'){
        it(nizPodataka[i].operacija + nizPodataka[i].ruta, (done) => {
            chai.request(server)
                .get(nizPodataka[i].ruta)
                .end((err,res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(nizPodataka[i].izlaz);
                done();
                });
        });
    }else if(nizPodataka[i].operacija == 'POST' && nizPodataka[i].ruta == '/predmet'){
        it(nizPodataka[i].operacija + nizPodataka[i].ruta, (done) => {
            chai.request(server)
                .post(nizPodataka[i].ruta)
                .send({naziv:nizPodataka[i].ulaz})
                .end((err,res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql(nizPodataka[i].izlaz);
                done();
                });
        });
    }else if(nizPodataka[i].operacija == 'POST' && nizPodataka[i].ruta == '/aktivnost'){
        it(nizPodataka[i].operacija + nizPodataka[i].ruta, (done) => {
            chai.request(server)
                .post(nizPodataka[i].ruta)
                .send(nizPodataka[i].ulaz)
                .end((err,res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql(nizPodataka[i].izlaz);
                done();
                });
        });
    }else if(nizPodataka[i].operacija == 'DELETE'){
        it(nizPodataka[i].operacija + nizPodataka[i].ruta, (done) => {
            chai.request(server)
                .delete(nizPodataka[i].ruta)
                .end((err,res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql(nizPodataka[i].izlaz);
                done();
                });
        });
    }
}


describe('Spirala 3 Testovi', function(){
    for(i=0; i<nizPodataka.length; i++){
       test(i);
    }
});
