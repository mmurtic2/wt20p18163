const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018163","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.predmet = require(__dirname+'/predmet.js')(sequelize, Sequelize.DataTypes);
db.grupa = require(__dirname+'/grupa.js')(sequelize, Sequelize.DataTypes);
db.aktivnost = require(__dirname+'/aktivnost.js')(sequelize, Sequelize.DataTypes);
db.dan = require(__dirname+'/dan.js')(sequelize, Sequelize.DataTypes);
db.tip = require(__dirname+'/tip.js')(sequelize, Sequelize.DataTypes);
db.student = require(__dirname+'/student.js')(sequelize, Sequelize.DataTypes);

//relacije
db.predmet.hasMany(db.grupa);
db.grupa.belongsTo(db.predmet);

db.predmet.hasMany(db.aktivnost, {foreignKey:{allowNull: false}});
db.aktivnost.belongsTo(db.predmet);

db.aktivnost.belongsTo(db.grupa);
db.grupa.hasMany(db.aktivnost);

db.dan.hasMany(db.aktivnost, {foreignKey:{allowNull: false}});
db.aktivnost.belongsTo(db.dan);

db.tip.hasMany(db.aktivnost, {foreignKey:{allowNull: false}});
db.aktivnost.belongsTo(db.tip);

db.grupa.belongsToMany(db.student,{as:'studenti',through:'student_grupa',foreignKey:'GrupaId'});
db.student.belongsToMany(db.grupa,{as:'grupe',through:'student_grupa',foreignKey:'StudentId'});

module.exports = db;